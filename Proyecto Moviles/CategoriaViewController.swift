//
//  CategoriaViewController.swift
//  Proyecto Moviles
//
//  Created by user152425 on 4/26/20.
//  Copyright © 2020 Alberto Rodríguez Torres. All rights reserved.
//

import UIKit
import CoreData

class CategoriaViewController: UITableViewController {

    @IBOutlet weak var nameView: UINavigationItem!
    var categoria: Categorias?
    
    var manager: CoreDataStack
    var managedContext: NSManagedObjectContext
    
    required init?(coder decoder: NSCoder){
        self.manager = CoreDataStack(modelName: "Datos")
        self.managedContext = self.manager.managedContex
        super.init(coder: decoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //En caso de no ser nulo se le da el nombre a la vista de la categoria en la que estoy
        nameView.title = categoria?.nombre
        // Do any additional setup after loading the view.
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let series = categoria?.series else {
            return 1
        }
        return series.count
    }
    
    //Rellena la tabla con las series pertenecientes a la categoria
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "SerieTableViewCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? SerieTableViewCell else {
            fatalError("The dequeued cell is not an instance of MealTableViewCell.")
        }
        let serie = categoria?.series?[indexPath.row] as! Series
        cell.nombreSerie.text = serie.titulo
        if let imagen = UIImage(data:(serie.imagen as Data? ?? Data()), scale:10.0){
            cell.imagen.image = imagen
        } else {
            cell.imagen.image = UIImage()
        } //TODO: arreglar casteo imagenes
        cell.sinopsis.text = serie.sinopsis
        return cell
    }
    
    //Funcion encargada de eliminar series de la categoria
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        //Se obtiene la serie seleccionada
        guard let serieAEliminar = categoria?.series?[indexPath.row] as? Series, editingStyle == .delete else {
            return
        }
        //Se borra de CoreData
        managedContext.delete(serieAEliminar)
        do{
            try managedContext.save()
            //Se borra de la tabla
            tableView.deleteRows(at: [indexPath], with: .automatic)
        } catch let error as NSError{
            print("Ocurrio un error al borrar \(error)")
        }
    }
    
    // MARK: - Navigation
    //Agrega una nueva serie si no estaba o la modifica si ya existe
     @IBAction func addSerie(sender: UIStoryboardSegue){
        if let sourceViewController = sender.source as? EditarSerieViewController {
            if(manager.getSerieByTitulo(Titulo: sourceViewController.Titulo.text as String!) != nil){
                //Esta funcion actualiza la serie en caso de existir
                categoria?.addToSeries(sourceViewController.serie as Series!)
                print("Serie modificada")
                do{
                    try managedContext.save()
                }catch let error as NSError{
                    print("Ocurrio un error: \(error.localizedDescription)")
                }
            } else {
                //Aqui primero se crea la serie y posteriormente se une a la categoria correspondiente
                manager.createSerie(titulo: sourceViewController.Titulo.text!, director: sourceViewController.Director.text!, capitulos: sourceViewController.Capitulos.text!, estado: sourceViewController.StatusData[sourceViewController.Status.selectedRow(inComponent: 0)], fechaInicio: sourceViewController.StartDate.date as NSDate, frecuencia: sourceViewController.FrecuenciaData[sourceViewController.Frecuencia.selectedRow(inComponent: 0)], imagen: NSData(), sinopsis: sourceViewController.Sipnosis.text, valoracion: Int16(sourceViewController.Nota.rating.description)!, context: managedContext)
                categoria?.addToSeries(manager.getSerieByTitulo(Titulo: sourceViewController.Titulo.text as String!)!)
                print("Serie agregada")
                do{
                    try managedContext.save()
                }catch let error as NSError{
                    print("Ocurrio un error: \(error.localizedDescription)")
                }
            }
            //Se recarga la tabla para actualizar datos
            self.tableView.reloadData()
        }
     }
    
    //En caso de seleccionar una serie envio cual a la siguiente vista
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        guard let serieTableCell = segue.destination as? SerieMainViewController else {
            return
        }
        guard let selectedCell = sender as? SerieTableViewCell else {
            fatalError("Unexpected sender")
        }
        guard let indexPath = self.tableView.indexPath(for: selectedCell) else {
            fatalError("The selected cell is not being displayed by the table")
        }
        let selectedSerie = categoria?.series?[indexPath.row] as! Series
        serieTableCell.serie = selectedSerie
        serieTableCell.allowEdit = true
    }
}
