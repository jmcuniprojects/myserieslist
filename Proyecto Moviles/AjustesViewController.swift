//
//  AjustesViewController.swift
//  Proyecto Moviles
//
//  Created by Alberto Rodríguez Torres on 11/05/2020.
//  Copyright © 2020 Alberto Rodríguez Torres. All rights reserved.
//

import UIKit
import CoreData

class AjustesViewController: UIViewController {
    
    var manager: CoreDataStack!
    var managedContext:NSManagedObjectContext!
    var notificationsActive: Bool!
    @IBOutlet weak var showNotifications: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        notificationsActive = true
        //Inicializo los valores del CoreData
        manager = CoreDataStack(modelName: "Datos")
        managedContext = manager.managedContex
    }
    
    //Carga Datos de ejemplo para comprobar funcionalidad
    @IBAction func importData(_ sender: Any) {
        //Crea categorias de ejemplo
        manager.createCategoria(nombre: "Reality Shows")
        manager.createCategoria(nombre: "Peliculas")
        manager.createCategoria(nombre: "Anime")
        let categoria1 = manager.getCategoria(nombre: "Reality Shows")
        let categoria2 = manager.getCategoria(nombre: "Peliculas")
        let categoria3 = manager.getCategoria(nombre: "Anime")
        
        //Agrego series a la categoria 1
        manager.createSerie(titulo: "Pesadilla en la Cocina T1", director: "Who Cares", capitulos: "16", estado: "Finalizada", fechaInicio: NSDate(timeIntervalSinceReferenceDate: TimeInterval(exactly: 7*31104000)!), frecuencia: "Semanal", imagen: NSData(), sinopsis: "Alberto Chicote liandola por ahi", valoracion: Int16(4), context: managedContext)
        manager.createSerie(titulo: "Operacion Triunfo T12", director: "Who Cares2", capitulos: "?", estado: "En emision", fechaInicio: NSDate(), frecuencia: "Mensual", imagen: NSData(), sinopsis: "Gente que intenta cantar bien", valoracion: Int16(1), context: managedContext)
        categoria1?.addToSeries(manager.getSerieByTitulo(Titulo: "Pesadilla en la Cocina T1")!)
        categoria1?.addToSeries(manager.getSerieByTitulo(Titulo: "Operacion Triunfo T12")!)
        
        manager.createSerie(titulo: "Regreso al Futuro", director: "Robert Zemeckis", capitulos: "1", estado: "Finalizada", fechaInicio: NSDate(timeIntervalSince1970: TimeInterval(exactly: 15*12*31104000)!), frecuencia: "Mensual", imagen: NSData(), sinopsis: "El adolescente Marty McFly es amigo de Doc, un científico al que todos toman por loco. Cuando Doc crea una máquina para viajar en el tiempo, un error fortuito hace que Marty llegue a 1955, año en el que sus futuros padres aún no se habían conocido. Después de impedir su primer encuentro, deberá conseguir que se conozcan y se casen; de lo contrario, su existencia no sería posible.", valoracion: Int16(5), context: managedContext)
        manager.createSerie(titulo: "Emoji: La Pelicula", director: "Tony Leondis", capitulos: "1", estado: "Finalizada", fechaInicio: NSDate(timeIntervalSinceNow: TimeInterval(exactly: -3*12*31104000)!), frecuencia: "Mensual", imagen: NSData(), sinopsis: "Gene es un emoji que vive en Textopolis, una ciudad digital dentro del teléfono de su usuario Alex. Él es el hijo de dos emojis de apellido Meh llamado Mel y Mary, y es capaz de hacer múltiples expresiones a pesar de la educación de sus padres. Sus padres dudan de que vaya a trabajar, pero Gene insiste ya que quiere sentirse útil.", valoracion: Int16(0), context: managedContext)
        categoria2?.addToSeries(manager.getSerieByTitulo(Titulo: "Regreso al Futuro")!)
        categoria2?.addToSeries(manager.getSerieByTitulo(Titulo: "Emoji: La Pelicula")!)
        
        do{
            try managedContext.save()
            
            if(showNotifications.isOn){
                let alert = UIAlertController(title: "Cargado de Datos", message: "Datos cargados correctamente. Por favor recargue la App para que se visualicen los cambios", preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                alert.addAction(defaultAction)
                present(alert, animated: true, completion: nil)
            }
        }catch let error as NSError{
            print("Error!! \(error.localizedDescription)")
        }
    }
    
    //Limpia todo el CoreData
    @IBAction func cleanData(_ sender: Any) {
        let categoriasFetch : NSFetchRequest<Categorias> = Categorias.fetchRequest()
        do{
            let aux = try managedContext.fetch(categoriasFetch)
            if(aux.count > 0){
                for cat in aux{
                    for serie in (cat.series)!{
                        let serieF = serie as! Series
                        managedContext.delete(serieF)
                    }
                    managedContext.delete(cat)
                }
            }
            try managedContext.save()
            
            if(showNotifications.isOn){
                let alert = UIAlertController(title: "Borrado de datos", message: "Datos borrados correctamente. Por favor recargue la App para que se visualicen los cambios", preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                alert.addAction(defaultAction)
                present(alert, animated: true, completion: nil)
            }
            
        }catch let error as NSError{
            print("Error!! \(error.localizedDescription)")
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
