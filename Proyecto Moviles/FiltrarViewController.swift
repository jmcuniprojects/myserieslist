//
//  FiltrarViewController.swift
//  Proyecto Moviles
//
//  Created by user152425 on 5/9/20.
//  Copyright © 2020 Alberto Rodríguez Torres. All rights reserved.
//

import UIKit
import CoreData

class FiltrarViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    let coreDataStack = CoreDataStack(modelName: "Datos")
    var managedContext: NSManagedObjectContext!
    
    @IBOutlet weak var categoriasTableView: UITableView!
    @IBOutlet weak var valoracionTableView: UITableView!
    @IBOutlet weak var estadosTableView: UITableView!
    @IBOutlet weak var aplicarButton: UIBarButtonItem!
    
    var categorias = [Categorias]()
    var estados = ["Visto", "En emision", "Finalizada", "Proximamente"]
    
    //Estructuras para almacenar los filtros
    var categoriasFiltradas = [String]()
    var valoracionLimite: Int?
    var estadosFiltrados = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        managedContext = coreDataStack.managedContex //Le doy un valor al contexto
        
        //Carga categorias en el CoreData
        let categoriasFetch : NSFetchRequest<Categorias> = Categorias.fetchRequest()
        do{
            let aux = try managedContext.fetch(categoriasFetch)
            
            if(aux.count > 0){
                categorias = aux
            }
        }catch let error as NSError{
            print("Error!! \(error.localizedDescription)")
        }
        
        self.categoriasTableView.allowsMultipleSelection = true
        self.categoriasTableView.allowsMultipleSelectionDuringEditing = true
        self.estadosTableView.allowsMultipleSelection = true
        self.estadosTableView.allowsMultipleSelectionDuringEditing = true
        
        self.categoriasTableView.reloadData()
        if(!categoriasFiltradas.isEmpty){
            var i = 0
            while(i < categorias.count){
                if(categoriasFiltradas.contains(categorias[i].nombre!)){
                    categoriasTableView.selectRow(at: IndexPath(row: i, section: 0), animated: false, scrollPosition: UITableView.ScrollPosition.none)
                }
                i+=1
            }
        }
        if(!estadosFiltrados.isEmpty){
            var i = 0
            while(i < estados.count){
                if(estadosFiltrados.contains(estados[i])){
                    estadosTableView.selectRow(at: IndexPath(row: i, section: 0), animated: false, scrollPosition: UITableView.ScrollPosition.none)
                }
                i+=1
            }
        }
    }

    //MARK: Table View Functions
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == categoriasTableView){
            return categorias.count
        } else if (tableView == valoracionTableView){
            return 1
        } else {
            return estados.count
        }
    }
    
    //Funcion que da valor a las celdas
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(tableView != valoracionTableView){
            let cellIdentifier = "MultipleCell"
            guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? FiltrarLabelTableViewCell  else {
                    fatalError("Imposible cargar celda")
                }
                if(tableView == categoriasTableView){
                    let categoria = categorias[indexPath.row]
                    cell.Label.text = categoria.nombre
                    if(categoriasFiltradas.contains(categoria.nombre!)){
                        cell.isSelected = true
                    }
                    return cell
                } else {
                    cell.Label.text = estados[indexPath.row]
                    if(estadosFiltrados.contains(cell.Label.text!)){
                        cell.isSelected = true
                    }
                    return cell
                }
        } else {
            guard let cell2 = tableView.dequeueReusableCell(withIdentifier: "NotaCell", for: indexPath) as? FiltrarNotaTableViewCell else {
                    fatalError("Imposible cargar celda")
                }
            if(valoracionLimite == nil){
                cell2.notaSlider.value = 0
                cell2.notaText.text = "0"
            } else {
                cell2.notaSlider.value = Float(valoracionLimite!)
                cell2.notaText.text = String(valoracionLimite!)
            }
            return cell2
        }
    }
    
    //Funcion para actualizar las estructuras que guardan los filtros
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(tableView != valoracionTableView){
            guard let cell = tableView.cellForRow(at: indexPath) as? FiltrarLabelTableViewCell else {return}
            if(tableView == categoriasTableView){
                if(!categoriasFiltradas.contains(cell.Label.text!)){
                    categoriasFiltradas.append(cell.Label.text!)
                }
            } else {
                if(!estadosFiltrados.contains(cell.Label.text!)){
                    estadosFiltrados.append(cell.Label.text!)
                }
            }
        } else {
            guard let cell = tableView.cellForRow(at: indexPath) as? FiltrarNotaTableViewCell else {return}
            cell.isSelected = false
        }
    }
    
    public func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if(tableView != valoracionTableView){
            guard let cell = tableView.cellForRow(at: indexPath) as? FiltrarLabelTableViewCell else {return}
            if(tableView == categoriasTableView){
                if(categoriasFiltradas.contains(cell.Label.text!)){
                    var i = 0
                    while(i < categoriasFiltradas.count){
                        if(categoriasFiltradas[i] == cell.Label.text!){
                            categoriasFiltradas.remove(at: i)
                            break
                        }
                        i+=1
                    }
                }
            } else{
                if(estadosFiltrados.contains(cell.Label.text!)){
                    var i = 0
                    while(i < estadosFiltrados.count){
                        if(estadosFiltrados[i] == cell.Label.text!){
                            estadosFiltrados.remove(at: i)
                            break
                        }
                        i+=1
                    }
                }
            }
        }
    }

    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        guard let button = sender as? UIBarButtonItem, button == aplicarButton else {
            return
        }
        //Guarda la valoracion ya que es lo unico que no se actualiza cuando cambia
        let cell = valoracionTableView.visibleCells[0] as! FiltrarNotaTableViewCell
        valoracionLimite = Int(cell.notaText.text!)
    }
}
