//
//  Categoria.swift
//  Proyecto Moviles
//
//  Created by user152425 on 4/24/20.
//  Copyright © 2020 Alberto Rodríguez Torres. All rights reserved.
//

import UIKit

class Categoria {
    //MARK: Properties
    var nombre: String
    var series = [Serie]()
    
    //MARK: Initiation
    init(nombre: String) {
        self.nombre = nombre
    }
    
    //MARK: Public Methods
    public func addSeries(serie: Serie){
        series += [serie]
    }
}
