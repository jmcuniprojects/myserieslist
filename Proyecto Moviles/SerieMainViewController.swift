//
//  SerieMainViewController.swift
//  Proyecto Moviles
//
//  Created by user152425 on 4/28/20.
//  Copyright © 2020 Alberto Rodríguez Torres. All rights reserved.
//

import UIKit

class SerieMainViewController: UIViewController {

    var serie : Series?
    var allowEdit: Bool?
    
    @IBOutlet weak var imagen: UIImageView!
    @IBOutlet weak var Director: UILabel!
    @IBOutlet weak var FechaInicio: UILabel!
    @IBOutlet weak var Frecuencia: UILabel!
    @IBOutlet weak var Estado: UILabel!
    @IBOutlet weak var Capitulos: UILabel!
    @IBOutlet weak var Sinopsis: UITextView!
    @IBOutlet weak var Valoracion: RatingControl!
    @IBOutlet weak var editButton: UIBarButtonItem!
    
    required init?(coder decoder : NSCoder){
        super.init(coder: decoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Carga los datos de la serie a visualizar puesto que la unica forma de acceder a la vista es mediante seleccionar una y por tanto tiene valor
        self.title = serie?.titulo
        if let imagen = UIImage(data:(serie?.imagen as Data? ?? Data()), scale:1.0){
            self.imagen.image = imagen
        } else {
            self.imagen.image = UIImage()
        }
        self.Director.text = serie?.director
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        self.FechaInicio.text = serie?.fechaInicio?.description//dateFormatter.string(from: serie.FechaInicio)
        self.Frecuencia.text = serie?.frecuencia
        self.Estado.text = serie?.estado
        self.Capitulos.text = serie?.capitulos
        self.Sinopsis.text = serie?.sinopsis
        self.Valoracion.rating = Int(serie?.valoracion.description as String!)!
        //Matiene habilitado o no la capacidad de editar la serie (Si se viene desde busqueda no se permite)
        if(allowEdit != nil){
            editButton.isEnabled = allowEdit!
        }
        
        self.reloadInputViews()
        // Do any additional setup after loading the view.
    }
    

    
    // MARK: - Navigation

    // En caso de querer editar asigno la serie actual
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        guard let serieView = segue.destination as? EditarSerieViewController else {
            return
        }
        serieView.serie = self.serie as Series!
    }
}
