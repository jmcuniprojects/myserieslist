//
//  Serie.swift
//  Proyecto Moviles
//
//  Created by user152425 on 4/24/20.
//  Copyright © 2020 Alberto Rodríguez Torres. All rights reserved.
//

import UIKit

class Serie {
    //MARK: Properties
    var Titulo: String
    var Director: String
    var FechaInicio: Date
    var Frecuencia: String
    var Estado: String
    var Capitulos: String
    var Sinopsis: String
    var Valoracion: Int
    
    var imagen: UIImage
    
    //MARK: Initiation
    init(Titulo: String) {
        self.Titulo = Titulo
        self.Director = ""
        self.FechaInicio = Date()
        self.Frecuencia = "Diaria"
        self.Estado = "Completada"
        self.Capitulos = "8/8"
        self.Sinopsis = ""
        self.Valoracion = 0
        imagen = UIImage()
    }
    
    init (Titulo: String, Director: String, FechaInicio: Date, Frecuencia: String, Estado: String, Capitulos: String, imagen: UIImage, sinopsis: String, valoracion: Int)
    {
        self.Titulo = Titulo
        self.Director = Director
        self.FechaInicio = FechaInicio
        self.Frecuencia = Frecuencia
        self.Estado = Estado
        self.Capitulos = Capitulos
        self.imagen = imagen
        self.Sinopsis = sinopsis
        self.Valoracion = valoracion
    }
    
    public func igualaSerie(serie: Serie){
        self.Titulo = serie.Titulo
        self.Director = serie.Director
        self.FechaInicio = serie.FechaInicio
        self.Frecuencia = serie.Frecuencia
        self.Estado = serie.Estado
        self.Capitulos = serie.Capitulos
        self.imagen = serie.imagen
        self.Sinopsis = serie.Sinopsis
        self.Valoracion = serie.Valoracion
    }
}
