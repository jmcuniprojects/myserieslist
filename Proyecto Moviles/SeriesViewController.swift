//
//  DataViewController.swift
//  Proyecto Moviles
//
//  Created by Alberto Rodríguez Torres on 19/04/2020.
//  Copyright © 2020 Alberto Rodríguez Torres. All rights reserved.
//

import UIKit
import CoreData
import os.log

class SeriesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{

    //MARK: Properties
    @IBOutlet weak var Categorias1: UITableView!
    var categorias = [Categorias]()
    
    var coreDataStack = CoreDataStack(modelName: "Datos")
    var managedContext : NSManagedObjectContext!
    
    override func loadView() {
        super.loadView()
        self.managedContext = coreDataStack.managedContex
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Consulto CoreData para mostrar las categorias existentes
        let categoriasFetch : NSFetchRequest<Categorias> = Categorias.fetchRequest()
        do{
            let aux = try managedContext.fetch(categoriasFetch)
            
            if(aux.count > 0){
                categorias = aux
            }else {
                //Si el CoreData esta vacio carga categoria de ejemplo con una serie "vacia"
                loadSugestedCat()
            }
        }catch let error as NSError{
            print("Error!! \(error.localizedDescription)")
        }
        
        //Recargamos la tabla para visualizar los nuevos datos
        self.Categorias1.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //Funcion para mostrar el pop up encargado de agregar categorias
    @IBAction func showPopUp(_ sender: Any) {
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "sbPopUpID") as! PopUpViewController
        self.addChild(popOverVC)
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParent: self)
    }
    
    //MARK: Private Methods
    //Metodo para cargar categoria de ejemplo
    private func loadSugestedCat(){
        let serie1 = Series(context: managedContext)
        serie1.titulo = "The Witcher"
        var categoria1 = Categorias(context: managedContext)
        categoria1.nombre = "Sugeridos"
        serie1.categoria = categoria1
        categoria1.addToSeries(serie1)
        categorias += [categoria1]
    }
    
    // MARK: - Table view data source
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categorias.count
    }
    
    //Funcion que da valor a las celdas
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "CategoriaTableViewCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CategoriaTableViewCell else{
            fatalError("Imposible generar celda")
        }
        
        let categoria = categorias[indexPath.row]
        cell.nombreCategoria.text = categoria.nombre
        
        return cell
    }
    
    //Funcion encargada de borrar categorias y sus series asociadas
    public func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        //Selecciona la categoria a eliminar
        guard let categoriaAEliminar = categorias[indexPath.row] as? Categorias, editingStyle == .delete else {
            return
        }
        
        //Se borran todas las series asociadadas (serian inaccesibles de otra forma)
        
        for serie in (categoriaAEliminar.series)!{
            let serieF = serie as! Series
            managedContext.delete(serieF)
        }
 
        //Se borra la categoria por ultimo
        managedContext.delete(categoriaAEliminar)
        
        //Guarda los cambios al CoreData
        do{
            try managedContext.save()
            print("Borrado de CoreData Satisfactorio")
            //Se borra la categoria de la tabla
            categorias.remove(at: indexPath.row)
            Categorias1.deleteRows(at: [indexPath], with: .automatic)
        } catch let error as NSError{
            print("Error borrando la categoria \(error)")
        }
    }
    
    //MARK: Action
    //Funcion para aãdir categorias desde el pop up
    @IBAction func addToCatList(sender: UIStoryboardSegue){
        if let sourceViewController = sender.source as? PopUpViewController {
            //Add Categoria
            if(sourceViewController.newCatName.text != ""){
                //Creo la categoria en el CoreData en el contexto actual e inserta en la tabla, finalmente se guardan los cambios
                coreDataStack.createCategoria(nombre: sourceViewController.newCatName.text!)
                let newIndexPath = IndexPath(row: categorias.count, section: 0)
                categorias.append(coreDataStack.getCategoria(nombre: sourceViewController.newCatName.text!)!)
                Categorias1.insertRows(at: [newIndexPath], with: .automatic)
                do{
                    try managedContext.save()
                }catch let error as NSError{
                    print("Error \(error.localizedDescription)")
                }
            }
        }
    }
    
    // MARK: - Navigation
    
    // Si se selecciona una categoria paso cual y el contexto actual a la siguiente vista
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        guard let categoriaTableViewController = segue.destination as? CategoriaViewController else {
            fatalError("Unexpected destination: \(segue.destination)")
        }
        guard let selectedCell = sender as? CategoriaTableViewCell else{
            fatalError("Unexpected sender: \(sender)")
        }
        guard let indexPath = Categorias1.indexPath(for: selectedCell) else{
            fatalError("The selected cell is not being displayed by the table")
        }
        let selectedCategoria = categorias[indexPath.row]
        categoriaTableViewController.categoria = selectedCategoria
        categoriaTableViewController.manager = self.coreDataStack
        categoriaTableViewController.managedContext = self.managedContext
    }
}

