//
//  SerieTableViewCell.swift
//  Proyecto Moviles
//
//  Created by user152425 on 4/26/20.
//  Copyright © 2020 Alberto Rodríguez Torres. All rights reserved.
//

import UIKit

class SerieTableViewCell: UITableViewCell { //Clase para almacenar las modificaciones visuales de la celda

    @IBOutlet weak var imagen: UIImageView!
    @IBOutlet weak var nombreSerie: UILabel!
    @IBOutlet weak var sinopsis: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
