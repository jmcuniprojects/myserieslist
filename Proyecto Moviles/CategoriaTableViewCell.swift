//
//  CategoriaTableViewCell.swift
//  Proyecto Moviles
//
//  Created by user152425 on 4/24/20.
//  Copyright © 2020 Alberto Rodríguez Torres. All rights reserved.
//

import UIKit

class CategoriaTableViewCell: UITableViewCell { //Clase para almacenar las modificaciones visuales de la celda
    //MARK: Properties
    @IBOutlet weak var nombreCategoria: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
