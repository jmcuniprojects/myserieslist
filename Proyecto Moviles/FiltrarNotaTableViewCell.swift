//
//  FiltrarNotaTableViewCell.swift
//  Proyecto Moviles
//
//  Created by user152425 on 5/9/20.
//  Copyright © 2020 Alberto Rodríguez Torres. All rights reserved.
//

import UIKit

class FiltrarNotaTableViewCell: UITableViewCell {

    @IBOutlet weak var notaSlider: UISlider!
    @IBOutlet weak var notaText: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func notaChanged(_ sender: Any) {
        notaText.text = String(Int(notaSlider!.value))
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
