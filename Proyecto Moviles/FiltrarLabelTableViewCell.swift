//
//  FiltrarLabelTableViewCell.swift
//  Proyecto Moviles
//
//  Created by user152425 on 5/9/20.
//  Copyright © 2020 Alberto Rodríguez Torres. All rights reserved.
//

import UIKit

class FiltrarLabelTableViewCell: UITableViewCell {

    @IBOutlet weak var Label: UILabel!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.accessoryType = selected ? .checkmark : .none
    }

}
