//
//  PopUpViewController.swift
//  Proyecto Moviles
//
//  Created by Alberto Rodríguez Torres on 21/04/2020.
//  Copyright © 2020 Alberto Rodríguez Torres. All rights reserved.
//

import UIKit
import CoreData
import os.log

class PopUpViewController: UIViewController {

    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var newCatName: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        
        self.showAnimate()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func closePopUp(_ sender: Any) {
        self.removeAnimate()
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
    
    //MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        //Comprobamos que se acepta crear una nueva categoria
        guard let button = sender as? UIButton, button === okButton else {
            //Es cancelar
            return
        }
    }
}
