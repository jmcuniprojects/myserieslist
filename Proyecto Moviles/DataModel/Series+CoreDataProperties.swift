//
//  Series+CoreDataProperties.swift
//  Proyecto Moviles
//
//  Created by user152425 on 5/2/20.
//  Copyright © 2020 Alberto Rodríguez Torres. All rights reserved.
//
//

import Foundation
import CoreData


extension Series {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Series> {
        return NSFetchRequest<Series>(entityName: "Series")
    }

    @NSManaged public var capitulos: String?
    @NSManaged public var director: String?
    @NSManaged public var estado: String?
    @NSManaged public var fechaInicio: NSDate?
    @NSManaged public var frecuencia: String?
    @NSManaged public var imagen: NSData?
    @NSManaged public var sinopsis: String?
    @NSManaged public var titulo: String?
    @NSManaged public var valoracion: Int16
    @NSManaged public var categoria: Categorias?

}
