//
//  Categorias+CoreDataProperties.swift
//  Proyecto Moviles
//
//  Created by user152425 on 5/2/20.
//  Copyright © 2020 Alberto Rodríguez Torres. All rights reserved.
//
//

import Foundation
import CoreData


extension Categorias {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Categorias> {
        return NSFetchRequest<Categorias>(entityName: "Categorias")
    }

    @NSManaged public var nombre: String?
    @NSManaged public var series: NSOrderedSet?

}

// MARK: Generated accessors for series
extension Categorias {

    @objc(insertObject:inSeriesAtIndex:)
    @NSManaged public func insertIntoSeries(_ value: Series, at idx: Int)

    @objc(removeObjectFromSeriesAtIndex:)
    @NSManaged public func removeFromSeries(at idx: Int)

    @objc(insertSeries:atIndexes:)
    @NSManaged public func insertIntoSeries(_ values: [Series], at indexes: NSIndexSet)

    @objc(removeSeriesAtIndexes:)
    @NSManaged public func removeFromSeries(at indexes: NSIndexSet)

    @objc(replaceObjectInSeriesAtIndex:withObject:)
    @NSManaged public func replaceSeries(at idx: Int, with value: Series)

    @objc(replaceSeriesAtIndexes:withSeries:)
    @NSManaged public func replaceSeries(at indexes: NSIndexSet, with values: [Series])

    @objc(addSeriesObject:)
    @NSManaged public func addToSeries(_ value: Series)

    @objc(removeSeriesObject:)
    @NSManaged public func removeFromSeries(_ value: Series)

    @objc(addSeries:)
    @NSManaged public func addToSeries(_ values: NSOrderedSet)

    @objc(removeSeries:)
    @NSManaged public func removeFromSeries(_ values: NSOrderedSet)

}
