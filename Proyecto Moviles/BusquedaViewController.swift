//
//  BusquedaViewController.swift
//  Proyecto Moviles
//
//  Created by user152425 on 5/8/20.
//  Copyright © 2020 Alberto Rodríguez Torres. All rights reserved.
//

import UIKit
import CoreData

class BusquedaViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    @IBOutlet var buscaTitulo: UISearchBar!
    var tituloBuscado: String?
    var searchActive: Bool!
    @IBOutlet weak var tableView: UITableView!
    
    var seriesTotales = [Series]()
    var seriesVisualizar = [Series]()
    
    //Booleanos usados para saber si hay valores para los distintos filtros y sus variables asociadas
    var filtroTitulo: Bool!
    var filtroCategoria: Bool!
    var categoriasFiltradas = [String]()
    var filtroValoracion: Bool!
    var valoracionLimite: Int!
    var filtroEstado: Bool!
    var estadosFiltrados = [String]()
    
    var coreDataStack = CoreDataStack(modelName: "Datos")
    var managedContext : NSManagedObjectContext!
    
    override func loadView() {
        super.loadView()
        searchActive = false
        filtroTitulo = false
        filtroCategoria = false
        filtroValoracion = false
        filtroEstado = false
        self.managedContext = coreDataStack.managedContex
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Carga todas las series y las pasa al array de la vista
        /*
        let categoriasFetch : NSFetchRequest<Categorias> = Categorias.fetchRequest()
        do{
            let aux = try managedContext.fetch(categoriasFetch)
            
            if(aux.count > 0){
                for cat in aux{
                    for serie in (cat.series)!{
                        let serieF = serie as! Series
                            seriesVisualizar.append(serieF)
                    }
                }
                //Preparo el array global
                seriesTotales = seriesVisualizar
            }
        }catch let error as NSError{
            print("Error!! \(error.localizedDescription)")
        }
         */
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.buscaTitulo.delegate = self
        self.tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        seriesVisualizar.removeAll()
        seriesTotales.removeAll()
        //Carga las series segun los filtros
        actualizaLista()
        self.tableView.reloadData()
    }
    
    //MARK: Private Functions
    private func actualizaLista(){
        let categoriasFetch : NSFetchRequest<Categorias> = Categorias.fetchRequest()
        do{
            let aux = try managedContext.fetch(categoriasFetch)
            if(aux.count > 0){
                for cat in aux{
                    //Comprueba si hay que aplicar filtro a las categorias y si es asi se hace
                    if(filtroCategoria){
                        if(categoriasFiltradas.contains(cat.nombre!)){
                            for serie in (cat.series)!{
                                let serieF = serie as! Series
                                var allow  = true//Variable que indica si se agrega la serie
                                //Aplica filtro a la valoracion si esta presente
                                if(filtroValoracion){
                                    if(serieF.valoracion < valoracionLimite){
                                        allow = false
                                    }
                                }
                                //Aplica filtro al estado si esta presente
                                if(filtroEstado){
                                    if(!estadosFiltrados.contains(serieF.estado!)){
                                        allow = false
                                    }
                                }
                                if(allow){
                                    seriesVisualizar.append(serieF)
                                }
                            }

                        }
                    } else {
                        for serie in (cat.series)!{
                            let serieF = serie as! Series
                            var allow  = true//Variable que indica si se agrega la serie
                            if(filtroValoracion){
                                if(serieF.valoracion < valoracionLimite){
                                    allow = false
                                }
                            }
                            if(filtroEstado){
                                if(!estadosFiltrados.contains(serieF.estado!)){
                                    allow = false
                                }
                            }
                            if(allow){
                                seriesVisualizar.append(serieF)
                            }
                        }
                    }
                }
                //Preparo el array global
                seriesTotales = seriesVisualizar
            }
        }catch let error as NSError{
            print("Error!! \(error.localizedDescription)")
        }
    }
    
    //MARK: Table View Functions
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return seriesVisualizar.count
    }

    //Funcion que da valor a las celdas
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "SerieTableViewCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? SerieTableViewCell else{
            fatalError("Imposible generar celda")
        }
        
        let serie = seriesVisualizar[indexPath.row]
        cell.nombreSerie.text = serie.titulo
        if let imagen = UIImage(data:(serie.imagen as Data? ?? Data()), scale:10.0){
            cell.imagen.image = imagen
        } else {
            cell.imagen.image = UIImage()
        }
        cell.sinopsis.text = serie.sinopsis
        return cell
    }
    
    //MARK: Search Functions
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        //Se comprueba que hay texto en la barra de busqueda, de otra forma se devuleve todo sin filtrar
        if(buscaTitulo.text?.isEmpty ?? false){
            seriesVisualizar = seriesTotales
            self.tableView.reloadData()
            return
        }
        //Si hay texto se filtra el array total segun la busqueda
        seriesVisualizar = seriesTotales.filter({ serie -> Bool in
            guard let search = buscaTitulo.text else {
                return false
            }
            return (serie.titulo?.contains(search))!
        })
        self.tableView.reloadData()
    }
    
    
    // MARK: - Navigation
    @IBAction func filtrarListado(sender: UIStoryboardSegue){
        if let vistaOrigen = sender.source as? FiltrarViewController{
            if(!vistaOrigen.categoriasFiltradas.isEmpty){
                self.filtroCategoria = true
                self.categoriasFiltradas = vistaOrigen.categoriasFiltradas
            } else {
                self.filtroEstado = false
            }
            if(vistaOrigen.valoracionLimite! != 0){
                self.filtroValoracion = true
                self.valoracionLimite = vistaOrigen.valoracionLimite!
            } else {
                self.filtroValoracion = false
            }
            if(!vistaOrigen.estadosFiltrados.isEmpty){
                self.filtroEstado = true
                self.estadosFiltrados = vistaOrigen.estadosFiltrados
            } else {
                self.filtroEstado = false
            }
        }
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if let serieTableCell = segue.destination as? SerieMainViewController {
            guard let selectedCell = sender as? SerieTableViewCell else {
                fatalError("Unexpected sender")
            }
            guard let indexPath = self.tableView.indexPath(for: selectedCell) else {
                fatalError("The selected cell is not being displayed by the table")
            }
            let selectedSerie = seriesVisualizar[indexPath.row]
            serieTableCell.serie = selectedSerie
            serieTableCell.allowEdit = false
        } else if let filtroView = segue.destination as? FiltrarViewController {
            if(filtroCategoria){
                filtroView.categoriasFiltradas = self.categoriasFiltradas
            } else {
                filtroView.categoriasFiltradas.removeAll()
            }
            if(filtroValoracion){
                filtroView.valoracionLimite = self.valoracionLimite
            } else {
                filtroView.valoracionLimite = 0
            }
            if(filtroEstado){
                filtroView.estadosFiltrados = self.estadosFiltrados
            } else {
                filtroView.estadosFiltrados.removeAll()
            }
        }
    }
    

}
