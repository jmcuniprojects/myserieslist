//
//  EditarSerieViewController.swift
//  Proyecto Moviles
//
//  Created by Alberto Rodríguez Torres on 28/04/2020.
//  Copyright © 2020 Alberto Rodríguez Torres. All rights reserved.
//

import UIKit

class EditarSerieViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var Titulo: UITextField!
    @IBOutlet weak var Imagen: UIImageView!
    @IBOutlet weak var Director: UITextField!
    @IBOutlet weak var Frecuencia: UIPickerView!
    @IBOutlet weak var Status: UIPickerView!
    @IBOutlet weak var Capitulos: UITextField!
    @IBOutlet weak var StartDate: UIDatePicker!
    @IBOutlet weak var Sipnosis: UITextView!
    @IBOutlet weak var Nota: RatingControl!
    @IBOutlet weak var SaveButton: UIBarButtonItem!
    var serie: Series?
    var StatusData: [String]
    var FrecuenciaData: [String]
    
    
    required init?(coder decoder: NSCoder)
    {
        StatusData = [String]()
        FrecuenciaData = [String]()
        super.init(coder: decoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Si la serie tiene un valor significa que vamos a editar por lo que carga datos de esta serie en los cuadros de texto
        if(serie?.titulo != nil){
            self.title = serie?.titulo
            self.Titulo.text = serie?.titulo
            self.Imagen.image = UIImage() // TODO: arreglar casteo imagen
            self.Director.text = serie?.director
            if(serie?.fechaInicio != nil){
                let fecha = serie?.fechaInicio! as Date!
                self.StartDate.date = fecha as Date!
            } else {
                self.StartDate.date = Date()
            }
            var aux = self.Frecuencia.index(ofAccessibilityElement: serie?.frecuencia ?? "Diaria" )
            self.Frecuencia.selectRow(aux, inComponent: 0, animated: false)
            aux = self.Status.index(ofAccessibilityElement: serie?.estado ?? "Finalizada")
            self.Status.selectRow(aux, inComponent: 0, animated: false)
            self.Capitulos.text = serie?.capitulos
            self.Sipnosis.text = serie?.sinopsis
            self.Nota.rating = Int(serie?.valoracion.description as String!)!
        }
        
        //Se delegan el control de los picker y se determinan variables
        self.Status.delegate = self
        self.Status.dataSource = self
        self.Frecuencia.delegate = self
        self.Frecuencia.dataSource = self

        StatusData = ["Finalizada", "En emisión", "Vista"]
        FrecuenciaData = ["Diaria", "Mensual", "Semanal"]
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return FrecuenciaData.count
    }
    
    // The data to return fopr the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
        var retorno: String?
        if (pickerView == Frecuencia){
            retorno = FrecuenciaData[row]
        }
        if (pickerView == Status){
            retorno = StatusData[row]
        }
        return retorno
    }
    
    @IBAction func addImage(_ sender: Any) {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        picker.sourceType = .photoLibrary
        present(picker, animated: true)
    }
    
    // MARK: - Navigation

    // En caso de querer guardar lo hecho se sobreescribe la serie con los valores dados por el usuario
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        super.prepare(for: segue, sender: sender)
        guard let button = sender as? UIBarButtonItem, button === SaveButton else {
            return
        }
        if (serie != nil)
        {
            serie?.titulo = Titulo.text
            serie?.director = Director.text
            serie?.fechaInicio = StartDate.date as NSDate
            serie?.frecuencia = FrecuenciaData[Frecuencia.selectedRow(inComponent: 0)]
            serie?.estado = StatusData[Status.selectedRow(inComponent: 0)]
            serie?.capitulos = Capitulos.text
            serie?.imagen = self.Imagen.image?.pngData() as NSData?
            serie?.sinopsis = self.Sipnosis.text
            serie?.valoracion = Int16(self.Nota.rating.description)!
        }
    }
}

extension EditarSerieViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let userPickedImage = info[.editedImage] as? UIImage else {
            return
        }
        self.Imagen.image = userPickedImage
        picker.dismiss(animated: true)
    }
}
