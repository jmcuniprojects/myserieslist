//
//  CoreDataStack.swift
//  Proyecto Moviles
//
//  Created by user152425 on 5/2/20.
//  Copyright © 2020 Alberto Rodríguez Torres. All rights reserved.
//

import Foundation
import CoreData

class CoreDataStack{ //Clase intermediaria entre el CoreData y las vistas
    private let modelName : String
    
    init(modelName: String) {
        self.modelName = modelName
    }
    //Variables autocalculables para obtener contenedor y contexto del CoreData
    private lazy var storeContainer : NSPersistentContainer = {
        let container = NSPersistentContainer(name: self.modelName)
        
        container.loadPersistentStores{
            (storesDescription, error) in
            if let error = error as NSError? {
                print("Error!! \(error.localizedDescription)")
            }
        }
        return container
    }()
    
    lazy var managedContex : NSManagedObjectContext = {
        return self.storeContainer.viewContext
    }()
    
    //Funcion para guardar los cambios (si los hay)
    func saveContext(){
        guard managedContex.hasChanges else {return}
        do{
            try managedContex.save()
        }catch let error as NSError {
            print("Error!! \(error.localizedDescription)")
        }
    }
    
    //Funcion para obtener las series con el titulo especificado
    func getSerieByTitulo(Titulo: String)-> Series?{
        do{
            let fetchRequest: NSFetchRequest<Series> = Series.fetchRequest()
            fetchRequest.predicate = NSPredicate.init(format: "%K == %@", #keyPath(Series.titulo), Titulo)
            
            let results = try managedContex.fetch(fetchRequest)
            if(results.count > 0){
                return results[0]
            }
        }catch let error as NSError {
            print("No se ha podido recuperar la serie \(error), \(error.userInfo)")
        }
        return nil
    }
    
    //Funcion para obtener la categoria segun su nombre
    func getCategoria(nombre: String)-> Categorias?{
        do{
            let fetchRequest: NSFetchRequest<Categorias> = Categorias.fetchRequest()
            fetchRequest.predicate = NSPredicate.init(format: "%K == %@", #keyPath(Categorias.nombre), nombre)
            
            let results = try managedContex.fetch(fetchRequest)
            if(results.count > 0){
                return results[0]
            }
        }catch let error as NSError {
            print("No se ha podido recuperar la categoria \(error), \(error.userInfo)")
        }
        return nil
    }
    
    //funcion para crear una categoria con el nombre especificacado
    func createCategoria(nombre: String) -> Bool{
        do{
            let entityNew = NSEntityDescription.entity(forEntityName: "Categorias", in: managedContex)!
            let categoria = NSManagedObject(entity: entityNew, insertInto: managedContex)
            categoria.setValue(nombre, forKey: "nombre")
            try managedContex.save()
            return true
        }catch let error as NSError {
            print("No se ha podido guardar. \(error), \(error.userInfo)")
        }
        return false
    }
    
    //Funcion para crear una serie con los datos especificados y en el contexto indicado
    func createSerie(titulo : String, director: String, capitulos: String, estado: String, fechaInicio: NSDate, frecuencia: String, imagen: NSData, sinopsis: String,valoracion: Int16, context: NSManagedObjectContext) -> Bool{
        do{
            let entityNew = NSEntityDescription.entity(forEntityName: "Series", in: context)!
            let serie = NSManagedObject(entity: entityNew, insertInto: context)
            serie.setValue(titulo, forKey: "titulo")
            serie.setValue(director, forKey: "director")
            serie.setValue(capitulos, forKey: "capitulos")
            serie.setValue(estado, forKey: "estado")
            serie.setValue(fechaInicio, forKey: "fechaInicio")
            serie.setValue(frecuencia, forKey: "frecuencia")
            serie.setValue(imagen, forKey: "imagen")
            serie.setValue(sinopsis, forKey: "sinopsis")
            serie.setValue(valoracion, forKey: "valoracion")
            
            try managedContex.save()
            return true
        }catch let error as NSError {
            print("No se ha podido guardar. \(error), \(error.userInfo)")
        }
        return false
    }
}
